﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yahtzee_Game
{
    public partial class Yahtzee_Game : Form
    {

        ScoreCard myScoreCard = new ScoreCard();
        int RollCounter = 0;

        bool diceOneClicked = false, diceTwoClicked = false, diceThreeClicked = false, diceFourClicked = false, diceFiveClicked = false;
        
        int diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue;

        private void dice4_Click(object sender, EventArgs e)
        {
            diceFourClicked = true;
            myScoreCard.tempArrayOfDice[3] = Convert.ToInt32(dice4.Text);
            dice4.BackColor = Color.LightBlue;
        }

        private void rollDice_Click(object sender, EventArgs e)
        {
            if (RollCounter < 3)
            {
                rollAllDice();
                RollCounter++;
            }
        }

        private void dice5_Click(object sender, EventArgs e)
        {
            diceFiveClicked = true;
            myScoreCard.tempArrayOfDice[4] = Convert.ToInt32(dice5.Text);
            dice5.BackColor = Color.LightBlue;
        }

        private void dice3_Click(object sender, EventArgs e)
        {
            if (!diceThreeClicked)
            {
                diceThreeClicked = true;
                myScoreCard.tempArrayOfDice[2] = Convert.ToInt32(dice3.Text);
                dice3.BackColor = Color.LightBlue;
            }
        }

        private void dice2_Click(object sender, EventArgs e)
        {
            diceTwoClicked = true;
            myScoreCard.tempArrayOfDice[1] = Convert.ToInt32(dice2.Text);
            dice2.BackColor = Color.LightBlue;
        }

        private void dice1_Click(object sender, EventArgs e)
        {
            if (!diceOneClicked)
            {
                diceOneClicked = true;
                myScoreCard.tempArrayOfDice[0] = Convert.ToInt32(dice1.Text);
                dice1.BackColor = Color.LightBlue;
            }
        }

        

        public void resetPlayTurn()
        {
            RollCounter = 0;
            for (int i = 0; i < 5; i++)
            {
                myScoreCard.tempArrayOfDice[i] = 0;
            }
            dice1.Text = "";
            dice2.Text = "";
            dice3.Text = "";
            dice4.Text = "";
            dice5.Text = "";
            diceOneClicked = false;
            diceTwoClicked = false;
            diceThreeClicked = false;
            diceFourClicked = false;
            diceFiveClicked = false;
            dice1.BackColor = Color.LightGray;
            dice2.BackColor = Color.LightGray;
            dice3.BackColor = Color.LightGray;
            dice4.BackColor = Color.LightGray;
            dice5.BackColor = Color.LightGray;
        }

       

        public void scoreButtonClick(object sender, EventArgs e)
        {


            int tempResult;
            string buttonText = ((Button)sender).Text;
            if (buttonText == "Total:")
            {
                

                totalField.Text = myScoreCard.totalScore.ToString();
            }
            else if (diceOneClicked && diceTwoClicked && diceThreeClicked && diceFourClicked && diceFiveClicked)
            {
                switch (buttonText)
                {
                    case "Ones":
                        tempResult = myScoreCard.callOnes(myScoreCard.scoreArray[0], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 0);
                        }
                        onesField.Text = myScoreCard.scoreArray[0].ToString();
                        resetPlayTurn();
                        break;
                    case "Twos":
                        tempResult = myScoreCard.callTwos(myScoreCard.scoreArray[1], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 1);
                        }
                        twosField.Text = myScoreCard.scoreArray[1].ToString();
                        resetPlayTurn();
                        break;
                    case "Threes":
                        tempResult = myScoreCard.callThrees(myScoreCard.scoreArray[2], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 2);
                        }
                        threesField.Text = myScoreCard.scoreArray[2].ToString();
                        resetPlayTurn();
                        break;
                    case "Fours":
                        tempResult = myScoreCard.callFours(myScoreCard.scoreArray[3], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 3);
                        }
                        foursField.Text = myScoreCard.scoreArray[3].ToString();
                        resetPlayTurn();
                        break;
                    case "Fives":
                        tempResult = myScoreCard.callFives(myScoreCard.scoreArray[4], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 4);
                        }
                        fivesField.Text = myScoreCard.scoreArray[4].ToString();
                        resetPlayTurn();
                        break;
                    case "Sixes":
                        tempResult = myScoreCard.callSixes(myScoreCard.scoreArray[5], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 5);
                        }
                        sixesField.Text = myScoreCard.scoreArray[5].ToString();
                        resetPlayTurn();
                        break;
                    case "Three Of A Kind":
                        tempResult = myScoreCard.callThreeOfAKind(myScoreCard.scoreArray[6], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 6);
                        }
                        threeOfAKindField.Text = myScoreCard.scoreArray[6].ToString();
                        resetPlayTurn();
                        break;
                    case "Four Of A Kind":
                        tempResult = myScoreCard.callFourOfAKind(myScoreCard.scoreArray[7], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 7);
                        }
                        fourOfAKindField.Text = myScoreCard.scoreArray[7].ToString();
                        resetPlayTurn();
                        break;
                    case "Full House":
                        tempResult = myScoreCard.callFullHouse(myScoreCard.scoreArray[8], myScoreCard.tempArrayOfDice);
                        if (tempResult > 0)
                        {
                            myScoreCard.addScore(25, 8);
                        }
                        fullHouseField.Text = myScoreCard.scoreArray[8].ToString();
                        resetPlayTurn();
                        break;
                    case "Small Straight":
                        tempResult = myScoreCard.callSmallStraight(myScoreCard.scoreArray[9], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 9);
                        }
                        smallStraightField.Text = myScoreCard.scoreArray[9].ToString();
                        resetPlayTurn();
                        break;
                    case "Large Straight":
                        tempResult = myScoreCard.callLargeStraight(myScoreCard.scoreArray[10], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 10);
                        }
                        largeStraightField.Text = myScoreCard.scoreArray[10].ToString();
                        resetPlayTurn();
                        break;
                    case "Chance":
                        tempResult = myScoreCard.callChance(myScoreCard.scoreArray[11], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 11);
                        }
                        chanceField.Text = myScoreCard.scoreArray[11].ToString();
                        resetPlayTurn();
                        break;
                    case "Yahtzee":
                        tempResult = myScoreCard.callYahtzee(myScoreCard.scoreArray[12], myScoreCard.tempArrayOfDice);
                        if (tempResult != -1)
                        {
                            myScoreCard.addScore(tempResult, 12);
                        }
                        yahtzeeFIeld.Text = myScoreCard.scoreArray[12].ToString();
                        resetPlayTurn();
                        break;
                }
            }
        }

        

        public Yahtzee_Game()
        {

            InitializeComponent();


            

            //Code used and changed from Stackoverflow
            onesButton.Click += new EventHandler(scoreButtonClick);
            twosButton.Click += new EventHandler(scoreButtonClick);
            threesButton.Click += new EventHandler(scoreButtonClick);
            foursButton.Click += new EventHandler(scoreButtonClick);
            fivesButton.Click += new EventHandler(scoreButtonClick);
            sixesButton.Click += new EventHandler(scoreButtonClick);
            threeOfAKindButton.Click += new EventHandler(scoreButtonClick);
            fourOfAKindButton.Click += new EventHandler(scoreButtonClick);
            fullHouseButton.Click += new EventHandler(scoreButtonClick);
            smallStraightButton.Click += new EventHandler(scoreButtonClick);
            largeStraightButton.Click += new EventHandler(scoreButtonClick);
            chanceButton.Click += new EventHandler(scoreButtonClick);
            yahtzeeButton.Click += new EventHandler(scoreButtonClick);
            total.Click += new EventHandler(scoreButtonClick);
        }

        public void rollAllDice()
        {
            Random random = new Random();
            if (!diceOneClicked)
            {
                diceOneValue = random.Next(1, 7);
                dice1.Text = diceOneValue.ToString();
            }

            if (!diceTwoClicked)
            {
                diceTwoValue = random.Next(1, 7);
                dice2.Text = diceTwoValue.ToString();
            }

            if (!diceThreeClicked)
            {
                diceThreeValue = random.Next(1, 7);
                dice3.Text = diceThreeValue.ToString();

            }

            if (!diceFourClicked)
            {
                diceFourValue = random.Next(1, 7);
                dice4.Text = diceFourValue.ToString();
            }

            if (!diceFiveClicked)
            {
                diceFiveValue = random.Next(1, 7);
                dice5.Text = diceFiveValue.ToString();
            }
        }
    }

    public class ScoreCard
    {
        public int totalScore = 0;
        public int[] scoreArray = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public int[] tempArrayOfDice = { 0, 0, 0, 0, 0 };
        public ScoreCard()
        {

        }

        public void addScore(int score, int indexOfScoreArray)
        {
            scoreArray[indexOfScoreArray] += score;
            totalScore += score;
        }

        public bool findVal(int num, int[] tempArrayOfDice)
        {
            for (int i = 0; i < 5; i++)
            {
                if (this.tempArrayOfDice[i] == num)
                {
                    return true;
                }
            }
            return false;
        }

        public int callOnes(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 1)
                    {
                        result++;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callTwos(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 2)
                    {
                        result += 2;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callThrees(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 3)
                    {
                        result += 3;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callFours(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 4)
                    {
                        result += 4;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callFives(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 5)
                    {
                        result += 5;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callSixes(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (tempArrayOfDice[i] == 6)
                    {
                        result += 6;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callThreeOfAKind(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                Array.Sort(tempArrayOfDice);
                for (int i = 0; i < 3; i++)
                {
                    if (tempArrayOfDice[i] == tempArrayOfDice[i + 1] &&
                        tempArrayOfDice[i + 1] == tempArrayOfDice[i + 2])
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            result += tempArrayOfDice[j];
                        }

                        i = 3;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callFourOfAKind(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                Array.Sort(tempArrayOfDice);
                for (int i = 0; i < 2; i++)
                {
                    if (tempArrayOfDice[i] == tempArrayOfDice[i + 1] &&
                        tempArrayOfDice[i + 1] == tempArrayOfDice[i + 2] &&
                        tempArrayOfDice[i + 2] == tempArrayOfDice[i + 3])
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            result += tempArrayOfDice[j];
                        }
                        i = 2;
                    }
                }
                return result;
            }
            return -1;
        }

        public int callFullHouse(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                Array.Sort(tempArrayOfDice);
                if ((tempArrayOfDice[0] == tempArrayOfDice[1] &&
                    tempArrayOfDice[2] == tempArrayOfDice[3] &&
                    tempArrayOfDice[3] == tempArrayOfDice[4]) ||
                    (tempArrayOfDice[0] == tempArrayOfDice[1] &&
                    tempArrayOfDice[1] == tempArrayOfDice[2] &&
                    tempArrayOfDice[3] == tempArrayOfDice[4]))
                {
                    return 25;
                }
                return result;
            }

            return -1;
        }

        public int callSmallStraight(int scoreArrayVal, int[] tempArrayOfDice)
        {

            
            int result = 0;
            if (scoreArrayVal == 0)
            {
                
                int counter = 0;
                for (int i = 1; i <= 4; i++)
                {
                    
                    if (this.tempArrayOfDice[0] == i ||
                        this.tempArrayOfDice[1] == i ||
                        this.tempArrayOfDice[2] == i ||
                        this.tempArrayOfDice[3] == i ||
                        this.tempArrayOfDice[4] == i)
                    {
                        counter++;
                    }
                }

                if (counter < 4)
                {
                    counter = 0;
                    for (int i = 2; i <= 5; i++)
                    {
                        if (findVal(i, tempArrayOfDice))
                        {
                            counter++;
                        }
                    }
                }

                if (counter < 4)
                {
                    counter = 0;
                    for (int i = 3; i <= 6; i++)
                    {
                        if (findVal(i, tempArrayOfDice))
                        {
                            counter++;
                        }
                    }
                }

                if (counter == 4)
                {
                    result = 30;
                    return result;
                }
                
            }
            return -1;
        }

        public int callLargeStraight(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                int counter = 0;
                for (int i = 1; i <= 5; i++)
                {
                    if (findVal(i, tempArrayOfDice))
                    {
                        counter++;
                    }
                }
                
                if (counter != 5)
                {
                    counter = 0;
                    for (int i = 2; i <= 6; i++)
                    {
                        if (findVal(i, tempArrayOfDice))
                        {
                            counter++;
                        }
                    }
                }

                if (counter == 5)
                {
                    result += 40;
                }
                return result;
            }

            return -1;
        }

        public int callChance(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    result += tempArrayOfDice[i];
                }
                return result;
            }
            return -1;
        }

        public int callYahtzee(int scoreArrayVal, int[] tempArrayOfDice)
        {
            int result = 0;
            if (scoreArrayVal == 0)
            {
                if (tempArrayOfDice[0] == tempArrayOfDice[1] &&
                    tempArrayOfDice[1] == tempArrayOfDice[2] &&
                    tempArrayOfDice[2] == tempArrayOfDice[3] &&
                    tempArrayOfDice[3] == tempArrayOfDice[4])
                {
                    result += 50;
                }
                return result;
            }
            return -1;
        }

    }

}
