﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yahtzee_Game;

namespace Unit_Test_Yahtzee_Game
{
    [TestClass]
    public class UnitTestScores
    {
        [TestMethod]
        public void TestOnes()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = {1, 1, 2, 3, 6};


            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callOnes(scoreArrayVal, tempArrayOfDice);


            //Assert
            Assert.AreEqual(tempHold, 2);
        }

        [TestMethod]
        public void TestTwos()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 1, 2, 2, 3, 5 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callTwos(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 4);
        }

        [TestMethod]
        public void TestThrees()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 1, 2, 3, 6, 3 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callThrees(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 6);
        }

        [TestMethod]
        public void TestFours()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 1, 4, 3, 4, 3 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callFours(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 8);
        }

        [TestMethod]
        public void TestFives()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 5, 4, 5, 4, 5 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callFives(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 15);
        }

        [TestMethod]
        public void TestSixes()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 5, 6, 6, 4, 6 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callSixes(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 18);
        }

        [TestMethod]
        public void TestThreeOfAKind()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 5, 4, 5, 4, 5 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callThreeOfAKind(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 23);
        }

        [TestMethod]
        public void TestFourOfAKind()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 5, 5, 5, 4, 5 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callFourOfAKind(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 24);
        }

        [TestMethod]
        public void TestFullHouse()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 5, 5, 4, 4, 5 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callFullHouse(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 25);
        }

        [TestMethod]
        public void TestSmallStraight()
        {
            //Arrange
            int scoreArrayVal = 0;

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            tempScoreCard.tempArrayOfDice[0] = 3;
            tempScoreCard.tempArrayOfDice[1] = 4;
            tempScoreCard.tempArrayOfDice[2] = 5;
            tempScoreCard.tempArrayOfDice[3] = 6;
            tempScoreCard.tempArrayOfDice[4] = 5;
            int tempHold = tempScoreCard.callSmallStraight(scoreArrayVal, tempScoreCard.tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 30);
        }

        [TestMethod]
        public void TestLargeStraight()
        {
            //Arrange
            int scoreArrayVal = 0;

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            tempScoreCard.tempArrayOfDice[0] = 2;
            tempScoreCard.tempArrayOfDice[1] = 3;
            tempScoreCard.tempArrayOfDice[2] = 4;
            tempScoreCard.tempArrayOfDice[3] = 5;
            tempScoreCard.tempArrayOfDice[4] = 6;
            int tempHold = tempScoreCard.callLargeStraight(scoreArrayVal, tempScoreCard.tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 40);
        }

        [TestMethod]
        public void TestChance()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 2, 6, 5, 5, 4 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callChance(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 22);
        }

        [TestMethod]
        public void TestYahtzee()
        {
            //Arrange
            int scoreArrayVal = 0;
            int[] tempArrayOfDice = { 4, 4, 4, 4, 4 };

            //Act
            ScoreCard tempScoreCard = new ScoreCard();
            int tempHold = tempScoreCard.callYahtzee(scoreArrayVal, tempArrayOfDice);

            //Assert
            Assert.AreEqual(tempHold, 50);
        }
    }
}
